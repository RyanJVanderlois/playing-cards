
// Playing Cards
// Mason Brull

#include <iostream>
#include <conio.h>

using namespace std;

enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum class Suit
{
	SPADES,
	DIAMONDS,
	CLUBS,
	HEARTS
};

struct Card
{
	Rank Rank;
	Suit Suit;
};
string rank_to_string(Rank type) {
	switch (type) {
	case 2:
		return "Two";
	case 3:
		return "Three";
	default:
		return "Unkonwn";
	}
}
string suit_to_string(Suit type) {
	switch (type) {
	case 0:
		return "hearts";
	case 1:
		return "diamonds";
	case 2:
		return "clubs";
	case 3:
		return "spades";
	default:
		return "Unkonwn";
	}
}
void PrintCard(Card card)
{
	std::cout << "The " << rank_to_string(card.rank) << " of " << suit_to_string(card.suit);
}
Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card2.rank > card1.rank)
	{
		return card2;
	}
	
}
int main()
{


	Card C1;
	C1.rank = Rank::two;
	C1.suit = Suit::clubs;
	PrintCard(C1);

	
	
	Card C2;
	C2.rank = Rank::ten;
	C2.suit = Suit::hearts;
	HighCard(C1, C2);
	_getch();
	return 0;
}
